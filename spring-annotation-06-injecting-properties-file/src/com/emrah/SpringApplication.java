package com.emrah;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringApplication {

	public static void main(String[] args) {
		var context = new ClassPathXmlApplicationContext("applicationContext.xml");
		var coach = context.getBean("baseballCoach", BaseballCoach.class);		
		System.out.println(coach.getDailyWorkout());
		System.out.println(coach.getTeam());
		System.out.println(coach.getEmail());
		context.close();
	}

}
