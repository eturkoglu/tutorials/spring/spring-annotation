package com.emrah;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.stereotype.Component;

@Component
public class BaseballCoach implements Coach {

	@Override
	public String getDailyWorkout() {
		return "Spend 30 minutes on batting practice";
	}
	
	@PostConstruct  //Javax-Annotation-Api jar is required
	public void initMethod() {
		System.out.println("Init method has been executed");
	}
	
	@PreDestroy //Javax-Annotation-Api jar is required
	public void destroyMethod() {
		System.out.println("Destroy method has been executed");
	}

}
