package com.emrah;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringApplication {

	public static void main(String[] args) {
		var context = new ClassPathXmlApplicationContext("applicationContext.xml");
		var coach = context.getBean("baseballCoach", Coach.class);		
		System.out.println(coach.getDailyWorkout());		
		context.close();
	}

}
