package com.emrah;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringApplication {

	public static void main(String[] args) {
		var context = new ClassPathXmlApplicationContext("applicationContext.xml");
		var coach1 = context.getBean("baseballCoach", Coach.class);
		var coach2 = context.getBean("baseballCoach", Coach.class);
		
		var result = (coach1 == coach2);
		
		System.out.println("Pointing the same object: " + result);
		
		System.out.println("\nMemory location of coach1: " + coach1);
		System.out.println("\nMemory location of coach2: " + coach2);
		
		context.close();
	}

}
